# IZ Build custom environment

## Pre-requisites

- [Install Salt on your VM](https://docs.saltstack.com/en/2017.7/contents.html)
- [Change ownership in common salt folders](https://docs.saltstack.com/en/2017.7/ref/configuration/nonroot.html). Try this command on your console:
```
chown -R vagrant /etc/salt /var/cache/salt /var/log/salt /var/run/salt
```


## Build environment for specific product/services

### AirBexPws

Summary: This recipe (sorry, I still tied to Chef terminology), "cooks" an environment for `air_bex_pws` connector. See the state definition for further data, but in few words, we checkout `master_apex_2` in all the services and dependencies and start all of them except by OVN. First run might take longer than usual, because we do a fetch looking for all new branches in repo. Then checkout to `master_apex_2` and then pull the changes in that branch. You can change the state execution order in case you need.

Salt command:
```
salt-call -c salt/configs state.apply amex_air_bex_pws.base -l debug
```

### HotelExpedia

Summary: This "state" definition, builds an environment for `hotel_expedia` connector. See the state definition for further data, but in few words, we checkout `apex_release_3` in all the services and dependencies and start all of them except by OVN. First run might take longer than usual, because we do a fetch looking for all new branches in repo. Then checkout to `apex_release_3` and then pull the changes in that branch. You can change the state execution order in case you need.

Salt command:
```
salt-call -c salt/configs state.apply amex_hotel_expedia.base  -l debug
```

### FlightExpedia

Summary: This "state" definition, builds an environment for `flight_expedia` connector. See the state definition for further data, but in few words, we checkout `air_responsive` in all the services and dependencies and start all of them except by OVN. Then checkout to `air_responsive` and then pull the changes in that branch. You can change the state execution order in case you need.

Salt command:
```
salt-call -c salt/configs state.apply amex_flight_expedia.base  -l debug
```

### DpBexPws

Summary: This "state" definition, builds an environment for `dp_bex_pws` connector. See the state definition for further data, but in few words, we checkout `apex_release_4` in all the services and dependencies and start all of them except by OVN. Then checkout to `apex_release_4` and then pull the changes in that branch. You can change the state execution order in case you need.

Salt command:
```
salt-call -c salt/configs state.apply amex_dp_bex_pws.base  -l debug
```
