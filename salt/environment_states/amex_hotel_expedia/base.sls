{% set api_path = '/home/vagrant/iseatz/api' %}

{% for repo, rev in salt['pillar.get']('HOTEL_EXPEDIA:REPOS').iteritems() %}
{{ repo }}-installation:
  cmd.run:
    - name: |
        git fetch
        git checkout {{ rev }}
    - cwd: {{ api_path }}/{{ repo }}
    - user: vagrant
{% endfor %}

start-logd:
  cmd.run:
    - name: bundle exec bin/logd start
    - cwd: {{ api_path }}/logd
    - user: vagrant

start-slog:
  cmd.run:
    - name: bundle exec bin/slog
    - cwd: {{ api_path }}/slog
    - user: vagrant

start-ovbs:
  cmd.run:
    - name: bundle exec bin/ovbs
    - cwd: {{ api_path }}/ovbs
    - user: vagrant
