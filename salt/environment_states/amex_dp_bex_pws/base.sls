{% set api_path = '/home/vagrant/iseatz/api' %}

{% for repo, rev in salt['pillar.get']('DP_BEX_PWS:REPOS').iteritems() %}
{{ repo }}-installation:
  cmd.run:
    - name: |
        git fetch
        git checkout {{ rev }}
    - cwd: {{ api_path }}/{{ repo }}
    - user: vagrant
{% endfor %}

start-logd:
  cmd.run:
    - name: bundle exec bin/logd start
    - cwd: {{ api_path }}/logd
    - user: vagrant

start-slog:
  cmd.run:
    - name: bundle exec bin/slog
    - cwd: {{ api_path }}/slog
    - user: vagrant

start-air-bex-pws:
  cmd.run:
    - name: bundle exec bin/air_bex_pws
    - cwd: {{ api_path }}/air_bex_pws
    - user: vagrant

start-air:
  cmd.run:
    - name: bundle exec bin/air
    - cwd: {{ api_path }}/air
    - user: vagrant

start-ovbs:
  cmd.run:
    - name: bundle exec bin/ovbs
    - cwd: {{ api_path }}/ovbs
    - user: vagrant
