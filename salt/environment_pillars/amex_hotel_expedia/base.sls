HOTEL_EXPEDIA:
  REPOS:
    pbr: apex_release_3
    one_view_service: apex_release_3
    ovn: apex_release_3
    hotel: apex_release_3
    hotel_expedia: apex_release_3
    ov_constants: apex_release_3
    ov_utils: apex_release_3
    slog: apex_release_3
    ovbs: apex_release_3
    logd: apex_release_3
